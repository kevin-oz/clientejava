/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.flota.flotaserver.rest;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Reserva;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.flotaserver.acceso.ReservaFacadeLocal;

/**
 *
 * @author kevin Figueroa
 */
@Path("reserva")
@RequestScoped
public class ReservaResource implements Serializable{
    
    @EJB
    ReservaFacadeLocal reservaFacade;
    
    
    /**
     * Contar registros
     *
     * @return el numero de registros que hay en la base de datos
     */
    @GET
    @Path("count")
    @Produces({MediaType.APPLICATION_JSON})
    public Response count() {
        if (reservaFacade != null) {
            try {
                int totalRegistros = reservaFacade.count();
                if (totalRegistros != 0) {
                    return Response.status(Response.Status.OK)
                            .entity(totalRegistros).build();
                }
            } catch (Exception e) {
            }

        }
        return Response.status(Response.Status.NOT_FOUND)
                .header("No se encontraron Registros", reservaFacade)
                .build();
    }
    /**
     * Buscar Todos
     *
     * @return todos los registros reservaFacade
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("all")
    public List<Reserva> findAll() {
        List<Reserva> lista = new ArrayList<>();
   
        if (reservaFacade != null) {
         
            try {
                if (reservaFacade.findAll() != null) {
                 
                    return lista = reservaFacade.findAll();
                }
            } catch (Exception e) {
                
            }
        }
        return lista = Collections.EMPTY_LIST;
    }

    
    /**
     * Buscar por un rango de registros
     *
     * @param primero
     * @param pagezise
     * @return
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Reserva> findRange(
            @QueryParam("inicio") @DefaultValue("0") int primero,
            @QueryParam("cantidad") @DefaultValue("3") int pagezise) {

        List<Reserva> ls;
        if (reservaFacade != null) {
            try {

                return ls = reservaFacade.findRange(primero, pagezise);

            } catch (Exception e) {
            }

        }

        return ls = Collections.EMPTY_LIST;
    }

    /**
     * Buscar registros por iD
     *
     * @param identificador id a buscar
     * @return
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findId(@PathParam("id") Long identificador) {

        Reserva buscado = reservaFacade.find(identificador);

        try {
            if (identificador >= 0 && buscado != null) {
                return Response.status(Response.Status.OK).entity(buscado).build();
            }
        } catch (Exception e) {
        }

        return Response.status(Response.Status.BAD_REQUEST).header("no se encontro registro:", identificador).build();
    }

//    /**
//     * Buscar coincidencias por nombre
//     *
//     * @param nombre parte del nombre a buscar
//     * @return lista de registros que coinciden con la busqueda
//     */
//    @GET
//    @Path("search")
//    @Produces({MediaType.APPLICATION_JSON})
//    public List<Reserva> searchLike(@QueryParam("name")String nombre) {
//        List<Reserva> lista_=null;
//        if (reservaFacade != null) {
//
//            return lista_ = reservaFacade.buscarPorNombre(nombre);
//       }
//
//        return lista_ = Collections.EMPTY_LIST;
//    }

    
    
        /**
     * Crear registros
     * @param registro a crear 
     * @return la entidad creada
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Reserva crear(Reserva registro){
       
        if(registro != null && registro.getIdReserva()== null){
            try {
                if (reservaFacade != null) {
                    Reserva nuevo = reservaFacade.crear(registro);
                    if(nuevo!=null){
                        return nuevo;
                    }else{
                        System.err.println("facade nulo");
                    }
                }
            } catch (Exception e) {
                System.out.println("ex: "+e);
            }
        }
        return new Reserva();
        
    }
    
    
    /**
     * Editar registros
     * @param registro, parametroa modificar
     * @return registro modificado
     */
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    public Reserva editar(Reserva registro){
        if(registro != null){
            try {
                if (reservaFacade != null && reservaFacade.find(registro.getIdReserva())!=null) {
                    Reserva nuevo = reservaFacade.editar(registro);
                    if(nuevo!=null){
                        return nuevo;
                    }
                }else{
                    System.out.println("no existe ese registro");
                }
            } catch (Exception e) {
                System.out.println("ex: "+e);
            }
        }
        return new Reserva();
    }
    
    /**
     * Eliminar registros
     * @param id identificadir del registro a eliminar
     * @return entidad eliminada
     */
    @DELETE
    @Path("{idTipoEstadoReserva}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Reserva eliminar(@PathParam("idTipoEstadoReserva") int id){
        if(id > 0){
            try {
                if (reservaFacade != null) {
                    Reserva die = reservaFacade.remover(reservaFacade.find(id));
                    if(die!=null){
                        return die;
                    }
                }
            } catch (Exception e) {
                System.out.println("ex: "+e);
            }
        }
        return new Reserva();
    }

   
    
}
