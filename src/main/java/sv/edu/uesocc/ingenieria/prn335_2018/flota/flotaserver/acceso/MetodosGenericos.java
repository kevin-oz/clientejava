/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.flota.flotaserver.acceso;

import java.util.List;

/**
 *
 * @author kevin
 * @param <T>
 */
public interface MetodosGenericos<T> {
   
    T crear(T entity);
    T editar(T entity);
    T remover(T entity);
    
    void create( T entity);

    void edit(T entity);

    void remove(T entity);

    T find(Object id);

    List<T> findAll();

    List<T> findRange(int inicio,int pagesize);

    int count();
    
    
}
