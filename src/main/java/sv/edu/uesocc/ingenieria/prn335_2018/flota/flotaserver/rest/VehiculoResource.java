/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.flota.flotaserver.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Vehiculo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.flotaserver.acceso.VehiculoFacadeLocal;

/**
 *
 * @author kevin
 */
@Path("vehiculo")
@RequestScoped
public class VehiculoResource implements Serializable{
    
    @EJB
    VehiculoFacadeLocal vehiculofacade;
    
    
     /**
     * Contar registros
     *
     * @return el numero de registros que hay en la base de datos
     */
    @GET
    @Path("count")
    @Produces({MediaType.APPLICATION_JSON})
    public Response count() {
        if (vehiculofacade != null) {
            try {
                int totalRegistros = vehiculofacade.count();
                if (totalRegistros != 0) {
                    return Response.status(Response.Status.OK)
                            .entity(totalRegistros).build();
                }
            } catch (Exception e) {
            }

        }
        return Response.status(Response.Status.NOT_FOUND)
                .header("No se encontraron Registros", vehiculofacade)
                .build();
    }

    /**
     * Buscar Todos
     *
     * @return todos los registros vehiculofacade
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("all")
    public List<Vehiculo> findAll() {
        List<Vehiculo> lista = new ArrayList<>();

        if (vehiculofacade != null) {
            try {
                if (vehiculofacade.findAll() != null) {
                    return lista = vehiculofacade.findAll();
                }
            } catch (Exception e) {
            }

        }
        return lista = Collections.EMPTY_LIST;
    }

    /**
     * Buscar por un rango de registros
     *
     * @param primero
     * @param pagezise
     * @return
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Vehiculo> findRange(
            @QueryParam("inicio") @DefaultValue("0") int primero,
            @QueryParam("cantidad") @DefaultValue("3") int pagezise) {

        List<Vehiculo> ls;
        if (vehiculofacade != null) {
            try {

                return ls = vehiculofacade.findRange(primero, pagezise);

            } catch (Exception e) {
            }

        }

        return ls = Collections.EMPTY_LIST;
    }

    /**
     * Buscar registros por iD
     *
     * @param identificador id a buscar
     * @return
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findId(@PathParam("id") Long identificador) {

        Vehiculo buscado = vehiculofacade.find(identificador);

        try {
            if (identificador >= 0 && buscado != null) {
                return Response.status(Response.Status.OK).entity(buscado).build();
            }
        } catch (Exception e) {
        }

        return Response.status(Response.Status.BAD_REQUEST).header("no se encontro registro:", identificador).build();
    }


     /**
     * Crear registros
     * @param registro a crear 
     * @return la entidad creada
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Vehiculo crear(Vehiculo registro){
       
        if(registro != null && registro.getIdVehiculo()== null){
            try {
                if (vehiculofacade != null) {
                    Vehiculo nuevo = vehiculofacade.crear(registro);
                    if(nuevo!=null){
                        return nuevo;
                    }else{
                        System.err.println("facade nulo");
                    }
                }
            } catch (Exception e) {
                System.out.println("ex: "+e);
            }
        }
        return new Vehiculo();
        
    }
    
    
    /**
     * Editar registros
     * @param registro, parametroa modificar
     * @return registro modificado
     */
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    public Vehiculo editar(Vehiculo registro){
        if(registro != null){
            try {
                if (vehiculofacade != null && vehiculofacade.find(registro.getIdVehiculo())!=null) {
                    Vehiculo nuevo = vehiculofacade.editar(registro);
                    if(nuevo!=null){
                        return nuevo;
                    }
                }else{
                    System.out.println("no existe ese registro");
                }
            } catch (Exception e) {
                System.out.println("ex: "+e);
            }
        }
        return new Vehiculo();
    }
    
    /**
     * Eliminar registros
     * @param id identificadir del registro a eliminar
     * @return entidad eliminada
     */
    @DELETE
    @Path("{idVehiculo}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Vehiculo eliminar(@PathParam("idVehiculo") Long id){
        if(id > 0){
            try {
                if (vehiculofacade != null) {
                    Vehiculo die = vehiculofacade.remover(vehiculofacade.find(id));
                    if(die!=null){
                        return die;
                    }
                }
            } catch (Exception e) {
                System.out.println("ex: "+e);
            }
        }
        return new Vehiculo();
    }

    
    
    
    
}
