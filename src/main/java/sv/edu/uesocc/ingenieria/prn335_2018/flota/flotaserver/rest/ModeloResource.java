/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.flota.flotaserver.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Modelo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.flotaserver.acceso.ModeloFacadeLocal;

/**
 *
 * @author kevin figueroa
 */
@Path("modelo")
@RequestScoped
public class ModeloResource implements Serializable {
    
    @EJB
    ModeloFacadeLocal modelofacade;
     
    /**
     * Buscar registros por iD
     *
     * @param identificador id a buscar
     * @return
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findId(@PathParam("id") Integer identificador) {

        Modelo buscado = modelofacade.find(identificador);

        try {
            if (identificador >= 0 && buscado != null) {
                return Response.status(Response.Status.OK).entity(buscado).build();
            }
        } catch (Exception e) {
        }
        return Response.status(Response.Status.BAD_REQUEST).header("no se encontro registro:", identificador).build();
    }

     /**
     * Buscar Todos
     *
     * @return todos los registros 
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("all")
    public List<Modelo> findAll() {
        List<Modelo> lista = new ArrayList<>();   
        if (modelofacade != null) {
            try {
                if (modelofacade.findAll() != null) {
                
                    return lista = modelofacade.findAll();
                }
            } catch (Exception e) {       
            }
        }
        return lista = Collections.EMPTY_LIST;
    }
}
